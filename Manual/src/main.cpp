/**
  ******************************************************************************
  * @file    main.cpp
  * @author  14
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

extern"C"{
#include <stdio.h>
#include <math.h>
#include "stm_lib.h"
#include "stm32f4xx.h"
#include "Dualshock3.h"
#include "MiniWhiteMD.hpp"
#include "SimpleUSART.h"
}

int v[3];
int v_arm;

/*Function------------------------------------------------------------------------------*/
volatile void calc_vel(int vx, int vy, int vr); /*
Calculation of velocity*/

volatile void wait(double tm);/*Stop program during "tm" seconds.*/


/*MiniWhiteMD setting-------------------------------------------------------------------*/

int up_limit = 900;
int low_limit = -900;

MiniWhiteMD FRW; /*Front Right Wheel--------*/
MiniWhiteMD FLW; /*Front Left Wheel---*/
MiniWhiteMD BCW; /*BuCk Wheel----*/
MiniWhiteMD ARM; /*ARm Motor----------*/

int main(void){
	/*main------------------------------------------------------------------------------*/
	int x, y, r;
	int adc_bit = pow( 2, 7); /* AD Converter's bit*/

	/*Setting---------------------------------------------------------------------------*/
	DS3_Init();
	SystemInit();
	SimpleUsart_Init();

	FRW.initMd(GPIOB, GPIO_Pin_6, TIM4, 1);
	FLW.initMd(GPIOB, GPIO_Pin_7, TIM4, 2);
	BCW.initMd(GPIOB, GPIO_Pin_8, TIM4, 3);
	ARM.initMd(GPIOB, GPIO_Pin_9, TIM4, 4);
	SimpleUsart_SendString("while\n");

    while(1){

    	x = 0;
    	y = 0;
    	r = 0;
    	v_arm = 0;

        /*Moving------------------------------------------------------------------------*/
        if(!DS3_IsNeutralLJoystick()){
        	x = (DS3_GetJoystick(DS3_JOY.L3X)-adc_bit/2) * 11;
        	y = -(DS3_GetJoystick(DS3_JOY.L3Y)-adc_bit/2) * 11;
        }
		/*Rotation----------------------------------------------------------------------*/
        else if(!DS3_IsNeutralRJoystick()){
			r = (DS3_GetJoystick(DS3_JOY.R3X)-adc_bit/2) * 5;
		}
		calc_vel(x, y, r);
		/*Arm----------------------------------------------------------------------------*/
		if(DS3_IsPressed(DS3_BTN.CIRCLE)){
			v_arm = 200;
		}
		else if(DS3_IsPressed(DS3_BTN.SQUARE)){
			v_arm = -200;
		}
		else if(DS3_IsPressed(DS3_BTN.DELTA)){
					v_arm = 400;
				}
		else if(DS3_IsPressed(DS3_BTN.CROSS)){
					v_arm = -400;
				}
		ARM.setMotor(v_arm);
    }
	return 0;
}

/*Function!!------------------------------------------------------------------------------*/

volatile void calc_vel(int vx, int vy, int vr){
	v[0] = (-vx/2+vy*sqrt(3)/2+vr);
	v[1] = (-vx/2-vy*sqrt(3)/2+vr);
	v[2] = vx + vr;
	for(int i=0;i<3;i++){
		if(v[i]>up_limit){
			v[i] = up_limit;
		}
		else if(v[i]<low_limit){
			v[i] = low_limit;
		}
	}
	FRW.setMotor(v[0]);
	FLW.setMotor(v[1]);
	BCW.setMotor(v[2]);
}


volatile void wait(double tm){
	int lim;
	lim = tm * 20000000;
	for(int i=0;i<lim;i++){}
}

