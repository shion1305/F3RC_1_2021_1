/**
 * @file	SimpleUSART-stbeemini.c
 * @version	1.0.1
 * @brief	SimpleUSARTライブラリのSTMF446reでの実装コード
 *
 * @warning	SIMPLEUSART_USE_USART系の定義によって使用するポートを変更することが可能な設計になっています。
 *
 */

#include "SimpleUSART.h"

#include "stm32f4xx_it.h"
#include "stdarg.h"

#ifdef SIMPLEUSART_USE_USART1
	// TX:PA9,  RX:PA10
	#define USARTx				(USART1)
#elif defined SIMPLEUSART_USE_USART2
	// TX:PA2,  RX:PA3
	#define USARTx				(USART2)
#elif defined SIMPLEUSART_USE_USART3
	// TX:PB10,PD8  RX:PB11/PD9
	#define USARTx				(USART3)
#elif defined SIMPLEUSART_USE_USART6
	// TX:PC6  RX:PC7
	#define USARTx				(USART6)
#else
	#error LyricalLib.SimpleUsart:UnselectedError「ふぇぇ…いずれかのUSARTを選択する必要があるよぉ」
#endif


// 自作vsprintf関数
#ifndef SIMPLEUSART_USE_STDLIB
	static int vtsprintf(char *buff, const char* fmt, va_list arg);				// 自作vsprintf関数
	static int tsprintf_decimal(signed int val, char* buff, int zf, int wd);	// 数値 => 10進文字列変換
	static int tsprintf_hexadecimal(unsigned int val, char* buff, int capital, int zf, int wd);// 数値 => 16進文字列変換
	static int tsprintf_char(int ch, char* buff);								// 数値 => 1文字キャラクタ変換
	static int tsprintf_string(char* str, char* buff);							// 数値 => ASCIIZ文字列変換
#endif



// 同期的な１バイトデータの送信
void SimpleUsart_SendByte(int8_t byte) {
	while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
	USART_SendData(USARTx, byte);
}


// 同期的な文字列の送信
// フォーマットを行わないので比較的高速
void SimpleUsart_SendString(const char* const str) {
	for (int i = 0; str[i] != '\0'; ++i) {
		SimpleUsart_SendByte(str[i]);
	}
}


// 同期的なフォーマット文字レズの送信
void SimpleUsart_Printf(const char* const fmt, ...) {
	va_list arg;
	va_start(arg, fmt);

	char buf[512];
#ifdef SIMPLEUSART_USE_STDLIB
	// vsprintfを用いたシンプルな方法
	// マイコン徹底入門環境下では undefined reference to `_sbrk' となりコンパイルエラー
	vsprintf(buf, fmt, arg);
#else
	// 自作関数であるvtsprintfを用いた方法
	vtsprintf(buf, fmt, arg);
#endif

	SimpleUsart_SendString(buf);
	va_end(arg);
	return;
}


// 初期化関数
void SimpleUsart_Init() {
	// USART4つ分をそれぞれ記述すると長くなるのでローカル変数を用いた (#defineを使っても良かったがスコープを限定したかった)
#ifdef SIMPLEUSART_USE_USART1			// TX:PA9,  RX:PA10
	const uint8_t		GPIO_AF_USARTx		= GPIO_AF_USART1;
	// TX: PA9
	GPIO_TypeDef* const	usartTxGpioPort		= GPIOA;
	const uint16_t		usartTxGpioPin		= GPIO_Pin_9;
	const uint8_t		usartTxGpioPinSource= GPIO_PinSource9;
	// RX: PA10
	GPIO_TypeDef* const	usartRxGpioPort		= GPIOA;
	const uint16_t		usartRxGpioPin		= GPIO_Pin_10;
	const uint8_t		usartRxGpioPinSource= GPIO_PinSource10;

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
#elif defined SIMPLEUSART_USE_USART2
	const uint8_t		GPIO_AF_USARTx		= GPIO_AF_USART2;
	// TX: PA2
	GPIO_TypeDef* const	usartTxGpioPort		= GPIOA;
	const uint16_t		usartTxGpioPin		= GPIO_Pin_2;
	const uint8_t		usartTxGpioPinSource= GPIO_PinSource2;
	// RX: PA3
	GPIO_TypeDef* const	usartRxGpioPort		= GPIOA;
	const uint16_t		usartRxGpioPin		= GPIO_Pin_3;
	const uint8_t		usartRxGpioPinSource= GPIO_PinSource3;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
#elif defined SIMPLEUSART_USE_USART3	// TX:PB10,  RX:PB11
	const uint8_t		GPIO_AF_USARTx		= GPIO_AF_USART3;
	// TX: PB10
	GPIO_TypeDef* const	usartTxGpioPort		= GPIOB;
	const uint16_t		usartTxGpioPin		= GPIO_Pin_10;
	const uint8_t		usartTxGpioPinSource= GPIO_PinSource10;
	// RX: PB11
	GPIO_TypeDef* const	usartRxGpioPort		= GPIOB;
	const uint16_t		usartRxGpioPin		= GPIO_Pin_11;
	const uint8_t		usartRxGpioPinSource= GPIO_PinSource11;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
#elif defined SIMPLEUSART_USE_USART6	// TX:PC6,  RX:PC7
	const uint8_t		GPIO_AF_USARTx		= GPIO_AF_USART6;
	// TX: PC6
	GPIO_TypeDef* const	usartTxGpioPort		= GPIOC;
	const uint16_t		usartTxGpioPin		= GPIO_Pin_6;
	const uint8_t		usartTxGpioPinSource= GPIO_PinSource6;
	// RX: PC7
	GPIO_TypeDef* const	usartRxGpioPort		= GPIOC;
	const uint16_t		usartRxGpioPin		= GPIO_Pin_7;
	const uint8_t		usartRxGpioPinSource= GPIO_PinSource7;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
#endif

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = usartTxGpioPin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(usartTxGpioPort, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = usartRxGpioPin;
	GPIO_Init(usartRxGpioPort, &GPIO_InitStructure);

	GPIO_PinAFConfig(usartTxGpioPort, usartTxGpioPinSource, GPIO_AF_USARTx);
	GPIO_PinAFConfig(usartRxGpioPort, usartRxGpioPinSource, GPIO_AF_USARTx);

	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USARTx, &USART_InitStructure);
	USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
	USART_Cmd(USARTx, ENABLE);
}



/*
 * 以下、自作vsprintf関数
 */
#ifndef SIMPLEUSART_USE_STDLIB
static int vtsprintf(char *buff, const char* fmt, va_list arg) {
    int len = 0, size = 0;
    int zeroflag, width;

    while(*fmt){
        if(*fmt=='%'){        /* % に関する処理 */
            zeroflag = width = 0;
            fmt++;

            if (*fmt == '0'){
                fmt++;
                zeroflag = 1;
            }
            if ((*fmt >= '0') && (*fmt <= '9')){
                width = *(fmt++) - '0';
            }
            /* printf ("zerof = %d,width = %d\n",zeroflag,width); */

            switch(*fmt){
            case 'd':        /* 10進数 */
                size = tsprintf_decimal(va_arg(arg,signed long),buff,zeroflag,width);
                break;
            case 'x':        /* 16進数 0-f */
                size = tsprintf_hexadecimal(va_arg(arg,unsigned long),buff,0,zeroflag,width);
                break;
            case 'X':        /* 16進数 0-F */
                size = tsprintf_hexadecimal(va_arg(arg,unsigned long),buff,1,zeroflag,width);
                break;
            case 'c':        /* キャラクター */
                size = tsprintf_char(va_arg(arg, int32_t),buff);
                break;
            case 's':        /* ASCIIZ文字列 */
                size = tsprintf_string(va_arg(arg, char*),buff);
                break;
            default:        /* コントロールコード以外の文字 */
                /* %%(%に対応)はここで対応される */
                len++;
                *(buff++) = *fmt;
                break;
            }
            len += size;
            buff += size;
            fmt++;
        } else {
            *(buff++) = *(fmt++);
            len++;
        }
    }

    *buff = '\0';        /* 終端を入れる */

    va_end(arg);
    return (len);
}

// 数値 => 10進文字列変換
static int tsprintf_decimal(signed int val,char* buff,int zf,int wd){
	int i;
	char tmp[10];
	char* ptmp = tmp + 10;
	int len = 0;
	int minus = 0;

	if (!val){		/* 指定値が0の場合 */
		*(ptmp--) = '0';
		len++;
	} else {
		/* マイナスの値の場合には2の補数を取る */
		if (val < 0){
			val = ~val;
			val++;
			minus = 1;
		}
		while (val){
			/* バッファアンダーフロー対策 */
			if (len >= 8){
				break;
			}

			*ptmp = (val % 10) + '0';
			val /= 10;
			ptmp--;
			len++;
		}
	}

	/* 符号、桁合わせに関する処理 */
	if (zf){
		if (minus){
			wd--;
		}
		while (len < wd){
			*(ptmp--) =  '0';
			len++;
		}
		if (minus){
			*(ptmp--) = '-';
			len++;
		}
	} else {
		if (minus){
			*(ptmp--) = '-';
			len++;
		}
		while (len < wd){
			*(ptmp--) =  ' ';
			len++;
		}
	}

	/* 生成文字列のバッファコピー */
	for (i=0;i<len;i++){
		*(buff++) = *(++ptmp);
	}

	return (len);
}
// 数値 => 16進文字列変換
static int tsprintf_hexadecimal(unsigned int val,char* buff, int capital,int zf,int wd){
	int i;
	char tmp[10];
	char* ptmp = tmp + 10;
	int len = 0;
	char str_a;

	/* A～Fを大文字にするか小文字にするか切り替える */
	if (capital){
		str_a = 'A';
	} else {
		str_a = 'a';
	}

	if (!val){		/* 指定値が0の場合 */
		*(ptmp--) = '0';
		len++;
	} else {
		while (val){
			/* バッファアンダーフロー対策 */
			if (len >= 8){
				break;
			}

			*ptmp = (val % 16);
			if (*ptmp > 9){
				*ptmp += str_a - 10;
			} else {
				*ptmp += '0';
			}

			val >>= 4;		/* 16で割る */
			ptmp--;
			len++;
		}
	}
	while (len < wd){
		*(ptmp--) =  zf ? '0' : ' ';
		len++;
	}
	for (i=0;i<len;i++){
		*(buff++) = *(++ptmp);
	}

	return(len);
}
// 数値 => 1文字キャラクタ変換
static int tsprintf_char(int ch, char* buff) {
	*buff = (char)ch;
	return(1);
}
// 数値 => ASCIIZ文字列変換
static int tsprintf_string(char* str,char* buff) {
	int count = 0;
	while(*str){
		*(buff++) = *str;
		str++;
		count++;
	}
	return(count);
}
#endif
