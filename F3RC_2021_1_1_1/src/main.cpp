/**
  ******************************************************************************
  * @file    main.cpp
  * @author  14
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

extern"C"{
#include <stdio.h>
#include <math.h>
#include "stm_lib.h"
#include "stm32f4xx.h"
#include "Dualshock3.h"
#include "MiniWhiteMD.hpp"
#include "SimpleUSART.h"
}

int v[3];
int v_arm;

/*Function------------------------------------------------------------------------------*/
volatile void calc_vel(int vx, int vy, int vr); /*
Calculation of velocity*/

volatile void wait(double tm);/*Stop program during "tm" seconds.*/


/*MiniWhiteMD setting-------------------------------------------------------------------*/

int up_limit = 900;
int low_limit = -900;

MiniWhiteMD MD[6];


int main(void){
	/*main------------------------------------------------------------------------------*/
	int x, y, r;
	int adc_bit = pow( 2, 7); /* AD Converter's bit*/

	/*Setting---------------------------------------------------------------------------*/
	DS3_Init();
	SystemInit();
	SimpleUsart_Init();



	MD[0].initMd(GPIOA, GPIO_Pin_15, TIM2, 1);

	SimpleUsart_SendString("while\n");

    while(1){


    }
	return 0;
}

/*Function!!------------------------------------------------------------------------------*/

volatile void calc_vel(int vx, int vy, int vr){
	v[0] = (-vx/2+vy*sqrt(3)/2+vr);
	v[1] = (-vx/2-vy*sqrt(3)/2+vr);
	v[2] = vx + vr;
	for(int i=0;i<3;i++){
		if(v[i]>up_limit){
			v[i] = up_limit;
		}
		else if(v[i]<low_limit){
			v[i] = low_limit;
		}
	}
	FRW.setMotor(v[0]);
	FLW.setMotor(v[1]);
	BCW.setMotor(v[2]);
}


volatile void wait(double tm){
	int lim;
	lim = tm * 20000000;
	for(int i=0;i<lim;i++){}
}

