/*
 * stm_lib.h
 *
 *  Created on: 2017/09/04
 *      Author: taiki
 *  ライブラリ依存をここで補い、他のファイルはライブラリ変更時に困らないようにする。
 */

#ifndef STM_LIB_H_
#define STM_LIB_H_
#if defined(STM32F334x8)
	#include "stm32f30x.h"
	#define MICON_CLOCK (8000)//(72000)
#elif defined(STM32F446xx)
	#include "stm32f4xx.h"
	#define MICON_CLOCK (180000)
#endif
#ifdef STM32F10X
	#include "stm32f10x.h"
	#define MICON_CLOCK (72000)
#endif

typedef GPIO_TypeDef GPIO_TYPEDEF;
typedef TIM_TypeDef  TIM_TYPEDEF;

uint8_t GPIO_Pin_Source(uint16_t GPIO_PIN);
uint32_t GPIO_RCC(GPIO_TypeDef* gpio_type);
uint32_t TIM_RCC(TIM_TypeDef* tim_type);

typedef struct{
	int pin_num;
	GPIO_TypeDef*  GPIO_PORT[16];
	uint16_t GPIO_PIN[16];
}GPIO_Output_Struct;
void GPIO_Output_Config(GPIO_Output_Struct* output);
void GPIO_HL(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, int a);

typedef struct{
	const int pin_num;
	GPIO_TypeDef*  GPIO_PORT[16];
	uint16_t GPIO_PIN[16];
}GPIO_Input_Struct;
void GPIO_Input_Config(GPIO_Input_Struct* input);
int GPIO_Input_Bit(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
int GPIO_Input_Data(GPIO_TypeDef* GPIOx);

typedef struct{
	const int pin_num;
	int ch_num[4];
	GPIO_TypeDef*  TIM_GPIO_PORT[4];//出力ポート
	uint16_t TIM_GPIO_PIN[4];//PWM出力するピン
	TIM_TypeDef* TIMx;
	uint32_t PWM_FREQUENCY;//pwm周波数(kHz)
	uint32_t PWM_MAX;//pwmの最大値
}TIM_Sim_PWM_Struct;
/*汎用タイマPWM出力用初期化*/
void TIM_Config_For_Simple_PWM(TIM_Sim_PWM_Struct* tim_pwm);

typedef struct{
	const int pin_num;
	int ch_num[3];
	GPIO_TypeDef*  TIM_GPIO_PORT[3][2];//出力ポート
	uint16_t TIM_GPIO_PIN[3][2];//PWM出力するピン
	TIM_TypeDef* TIMx;
	uint32_t PWM_FREQUENCY;//pwm周波数(kHz)
	uint32_t PWM_MAX;//pwmの最大値
}TIM_Adv_PWM_Struct;
/*高機能相補PWM出力用初期化*/
void TIM_Config_For_Advanced_PWM(TIM_Adv_PWM_Struct* tim_pwm);
void Set_Compare(int num, TIM_TypeDef* TIMx, int duty);

typedef struct{
	TIM_TypeDef* TIMx;
	int priority;//割り込み優先順位
	uint32_t TIM_PERIOD;//タイマ周期(単位:us)
}TIM_IRQ_Struct;

void TIM_Update_IRQ_Config(TIM_IRQ_Struct* tim_irq);
void TIM_NVIC_Config(TIM_IRQ_Struct* tim_irq);
void TIM_IRQ_Func_Init(TIM_IRQ_Struct* tim_irq, void (*func)(void));

//void TIM2_IRQHandler(void);
//void TIM3_IRQHandler(void);
//void TIM4_IRQHandler(void);
//void TIM6_DAC1_IRQHandler(void);
//void TIM7_DAC2_IRQHandler(void);
#endif /* STM_LIB_H_ */
