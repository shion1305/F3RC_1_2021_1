/**
 * @file	SimpleUSART.h
 * @version	1.0.0
 *
 * @brief	USARTの基本的なデバッグ用の送信モジュール
 *
 * 			USARTを用いて文字列やバイトデータを送信します。現状ではwhileなどの待機文が多用されていて
 * 			パフォーマンスにすぐれないためデバッグ用出力止まりです。入力についても整備していません
 *			
 *			また、ハンドラによる管理などをせずに使用するUSARTやピンについてはdefineで全てを制御している
 *			のでこのライブラリでは１つのUSARTのみしかアクセスできません。複数のUSARTを受信も含め柔軟に
 *			扱えるようにするUSARTx.h/cとかをそのうち作りたい…
 *			
 * @par			経緯
 * 			基本的に先代などの色々部室内に散らばっていたUSARTのプログラムを整理して私好みのプログラムに
 * 			変更した。F3RC 2016 で私が使っていたり色々あったが、今更にしてライブラリとして公開
 *
 * @attention
 * 			送信の際にwhileによる待機があるので、実行速度を無視できない場面での仕様は気をつけてね
 *
 *
 * @author	春日野穹
 * @since	2017/09/05 (SimpleUSARTとしてLyricalLibに追加)
 * @date	2017/09/05	ver.1.0.0	SimpleUSARTとしてLyricalLibraryに追加とそれに伴った命名の変更
 *
 */


#ifndef __SIMPLEUSART_H_
#define __SIMPLEUSART_H_
#include <stdint.h>

/*
 * 使用するUSART
 */
//#define SIMPLEUSART_USE_USART1
#define SIMPLEUSART_USE_USART2
//#define SIMPLEUSART_USE_USART3
//#define SIMPLEUSART_USE_USART6	// F4向け



/*
 * デバッグ用マクロ debugマクロの有効化
 */
#define DEBUG_MESSAGE



/*
 * 標準ライブラリ関数vsprintfを使用するには有効化のこと
 * (マイコン徹底入門環境下ではエラーが発生するので無効化推奨)
 */
//#define SIMPLEUSART_USE_STDLIB



/**
 * @brief	SimpleUSARTモジュールの初期化関数
 *
 *			このライブラリのモジュール
 *
 * @param [in]		byte	送信するデータ(1バイト分)
 * @return					なし
 */
void SimpleUsart_Init();



/**
 * @brief	１バイト分のデータを送信する関数
 *
 * @note	送信可能になるまで while の無限ループによる待機をする点に注意してください。
 *
 * @param [in]		byte	送信するデータ(1バイト分)
 * @return					なし
 */
void SimpleUsart_SendByte(int8_t byte);



/**
 * @brief	文字列を送信する関数
 *
 *			文字列を送信します。内部では SimpleUsart_SendByte 関数を繰り返し呼び出しています。
 *			SimpleUsart_Printf 関数に比べてフォーマット指定がないのでその分処理は少なく済みます
 *
 * @note	SendByte関数内のwhileによって時間がかかりやすくなる点に気をつけてください
 *
 * @param [in]		str		送信する文字列
 * @return					なし
 */
void SimpleUsart_SendString(const char* const str);



/**
 * @brief	フォーマット文字列を送信する関数
 *
 *			フォーマットされた文字列を送信します。printf関数と同様に使えます、デバッグ時などに用いる
 *			ことを想定しています。変数の出力などをする必要がないとき（引数が１つ）には単純に文字列の
 *			みを送信する SimpleUsart_SendString の方が若干高速であるためおすすめです。
 *
 *			C言語標準ライブラリのvsprintf関数が一部環境で使えないため、独自に実装したvtsprintf関数に
 *			よってフォーマット部分を実現しています。自作vsprintf関数の場合は以下のフォーマット指定の
 *			機能が使えます
 *				- %d による整数の10進数表記
 *				- %x による整数の16進数表記(小文字：0-9a-f)
 *				- %X による整数の16進数表記(大文字：0-9A-F)
 *				- %c によるASCII文字の表示
 *				- %s によるASCII文字列の表示(UTF8やShiftJISは未対応っぽい？)
 *
 * @note	SendByte関数内のwhileによって時間がかかりやすくなる点に気をつけてください
 *
 * @param [in]		fmt		送信する文字列のフォーマット
 * @return					なし
 */
void SimpleUsart_Printf(const char* const fmt, ...);



//	DEBUG_MESSAGEマクロが定義されている時にのみコンソール上にファイル名と行数を表示してくれるいいやつ
//	参考URL: http://qiita.com/tamy0612/items/3076b824bd4edf8d1429
/**
* @brief	デバッグ時のみ有効になるUSARTデバッグ出力
*
* 			DEBUG_MESSAGEマクロが定義されているときにのみ文字列が出力できるマクロ関数で、
*			実態は SimpleUsart_Printf なのでフォーマットによる変数の出力が可能。
*
* @note		ファイル名と行数が一緒表示され、末尾には改行が付きます
*/
#ifdef DEBUG_MESSAGE
	// DEBUG_MESSAGEが定義されているときはprintfデバッグ用関数化
	#define debug( fmt, ... ) \
		SimpleUsart_Printf("[%s:%d] " fmt "\n\r", \
									__FILE__, __LINE__, ##__VA_ARGS__ \
		)
#else /* !NDEBUG */
	// DEBUG_MESSAGEが定義されていないときは何もしない
	#define debug( fmt, ... ) ((void)0)
#endif /* NDEBUG */



#endif /* __SIMPLEUSART_H_ */
