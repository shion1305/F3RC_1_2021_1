/**
 * @file	Dualshock3.h
 * @version	2.0.1
 * @brief	Dualshock3(PS3コントローラー)をSBDBT経由で情報を取得するライブラリ
 *
 * 			詳細な使用方法・使用例に関しては同梱する main.c 及び 関数内コメントを参照してください。
 *
 *
 * @warning	DS3_USE_USART系の定義によって使用するポートを変更することが可能（ポートはSTBee mini仕様）
 * 			な設計になっていますが、USART1以外の初期化が未実装なままなのでそのままじゃ使えません。
 *
 *
 * @par			history（沿革／背景）
 *  		PS3_Bluetooth_new.hの問題点（個人的にアンチパターンとして採用してもいいと思う）
 *			- .hファイルのみで対になる.cファイルがそもそも無く、（.hはmain.cでinclude
 *			  される為）グローバル空間に関数が定義されてしまっていた
 *			- ライブラリ内のグローバル変数のスコープが広くなる（それを利用していた）
 *			- 公開する必要のない関数、変数、マクロがグローバル空間に公開されていた
 *			- その結果、副作用が起きやすい。
 *			- だけでなく、名前が衝突しやすい。
 *			- ボタンの判定で else if とその判定文での CmdData==mask のダブルコンボ
 *			- その結果として、同時押しに非対応
 *			- ところどころで見かける無意味な else とか else if とか continue
 *
 *			その後、石垣先輩が整理したソースもあったが一応自分でのものを作りたかった。
 *			ソースの作成に関しては、石垣先輩の整理後のものと上記の旧ファイルを参考にしま
 *			した。
 *  
 * @par			使用方法と旧ライブラリとの互換性
 *			上述したとおり、石垣先輩の整理したソースなども参考に書き換えたけれどほとんど原型をとどめて
 *  		いないため、互換性はまったくありません。仕様に関しては main.c をサンプルとして参考にしてく
 *  		ださい。テンプレとしては作成してませんが、使用方法に関して十分参考になると思います。
 *
 * @par			Dualshock2.h/cとの互換性
 * 			旧ライブラリとの互換性を無視した一方で有線コントローラのDualshock2.h/cとは互換性が高く、ポ
 * 			ートの設定などの一部を除けば、DS2とDS3を書き換えることでだいたい動くはずです。
 *
 * @par			SBDBTの主要ピンメモ
 * 			詳細については公式データシートを参照のこと
 * 			 2: VDD	3v
 * 			 3: GND
 * 			 6: RTS	フロー制御無効にするので9とショート
 * 			 7: TX
 * 			 8: RX
 * 			 9: CTS	6ピンの説明参照
 *
 * @author	春日野穹
 * @since	2016/11/05
 * @date	2016/11/21	ver.1.0.0β	射的機のリファクタリングに合わせて安定版作成
 * @date	2017/04/20	ver.2.0.0	gitで公開するに先立って、いろいろ変えた
 * @date	2017/09/08	ver.2.0.1	F4向けの実装に対応と微妙な機能追加
 *
 */

#ifndef __DUALSHOCK3_H_
#define __DUALSHOCK3_H_

#include <stdbool.h>

/**
 *  使用USART選択
 *  	USART1,2,3のどちらを使うかをここで指定します。どちらかを定義しないと#errorが発生します
 *  	現在ポートなどの設定は STBee mini 仕様なので適宜 Dualshock3.c に手を加えてください（ヘボ設計）
 */
//#define DS3_USE_USART1
//#define DS3_USE_USART2
//#define DS3_USE_USART3
#define DS3_USE_USART6	// F4向け


/**
 * 	unsafeモード
 * 		unsafeモードでは配列のゲッターで引数が要素内かをチェックしたりといった動作を省くことで、
 * 		若干の高速化を目指すマクロです。特に理由がない限りは利用しないことを推奨します。
 */
//#define DS3_UNSAFE



/**
 * 	割り込み使用モード
 * 		有効にすることが推奨ですが、割り込みが上手く動作せず使えない場合には以下をコメントアウトすることで、
 * 		代わりにDS3_Update関数で処理を行うためモジュールを使用することが可能になります。
 * 		実際にSBDBTから受信するデータはそれほど高頻度でなく、多くの場合DS3_Update関数の方が高頻度で呼び出さ
 * 		れます。そのため、DMA割り込み(IRQHandler)で受信データを処理する方が効率性を期待できます。
 */
#define DS3_USE_INTERRUPT


/// Dualshock3で取得できるコントローラー上のボタンの識別子（内部処理においては順番大事）
typedef enum {
	_DS3_ARRAY_START_ = 0,						// 配列の最初（一応）

	_DS3_UP = 0, _DS3_RIGHT, _DS3_DOWN, _DS3_LEFT,
	_DS3_START, _DS3_SELECT, /*_DS3_L3, _DS3_R3,*/
	_DS3_DELTA, _DS3_CIRCLE, _DS3_CROSS, _DS3_SQUARE,
	_DS3_L2, _DS3_R2, _DS3_L1, _DS3_R1,
	
	_DS3_ARRAY_NUM_								// 配列の最後＝このenumの要素数＝ボタン数＝16
} _DS3_ButtonID;

/// _DS3_ButtonID(enum)をラップする構造体型
typedef struct {
	_DS3_ButtonID id;
} DS3_ButtonID;

// DS3_ButtonID(enum型のラッパー構造体)の定数をまとめた構造体型
/**
 * @brief	DS3_ButtonIDの定数をまとめた定数として使う構造体
 *
 * 			DS3_ButtonID(ボタンの識別子として用いる構造体)の定数をまとめた構造体で、
 * 			externしているconst変数 DS3_BTN のための型。これにより、main.cなどで
 * 			DS3_BTN.UPなどという風にボタンを指定できるようになる。
 */
typedef struct {
	const DS3_ButtonID UP;
	const DS3_ButtonID RIGHT;
	const DS3_ButtonID DOWN;
	const DS3_ButtonID LEFT;
	const DS3_ButtonID START;
	const DS3_ButtonID SELECT;
	/*const DS3_ButtonID L3;*/
	/*const DS3_ButtonID R3;*/
	const DS3_ButtonID DELTA;
	const DS3_ButtonID CIRCLE;
	const DS3_ButtonID CROSS;
	const DS3_ButtonID SQUARE;
	const DS3_ButtonID L2;
	const DS3_ButtonID R2;
	const DS3_ButtonID L1;
	const DS3_ButtonID R1;
} _DS3_ButtonIDs;
extern const _DS3_ButtonIDs DS3_BTN;	//!< ボタンの識別子 DS3ButtonID を集めた定数

/// Dualshock3で取得できるジョイスティックの識別子
typedef enum {
	_DS3_JOY_START_ = 0,

	_DS3_L3X = 0, _DS3_L3Y,
	_DS3_R3X, _DS3_R3Y,

	_DS3_JOY_NUM_
} _DS3_JoystickID;


typedef struct {
	_DS3_JoystickID id;
} DS3_JoystickID;


typedef struct {
	const DS3_JoystickID L3X;
	const DS3_JoystickID L3Y;
	const DS3_JoystickID R3X;
	const DS3_JoystickID R3Y;
} _DS3_JoystickIDs;
extern const _DS3_JoystickIDs DS3_JOY;



/**
 * @brief	Dualshock3の初期化関数
 *
 * 			ほかの初期化関数と同様に初めの方で１度だけ呼び出してください。
 * 			ここでUSARTの初期化などが行われます
 *
 * @return	なし
 */
void DS3_Init();


/**
 * @brief	Dualshock3の更新関数
 *
 * 			これを１フレーム毎（while中やTIMx_IRQHandler）の冒頭で呼び出してください。
 *
 * @return	なし
 */
void DS3_Update();


/**
 * @brief	該当のボタンが押されているかのアクセサ
 *
 * @param [in]		btn		取得する対象のボタンを指定する
 * @return					押されている間はtrueが、そうでないときはfalseが返る
 * @retval			true	押されている
 * @retval			false	押されていな
 */
bool DS3_IsPressed(DS3_ButtonID btn);


/**
 * @brief	該当のボタンが押された瞬間かのアクセサ
 *
 * @param [in]		btn		取得する対象のボタンを指定する
 * @return					押された瞬間の１フレームだけtrueが返る。それ以外はfalse
 * @retval			true	押された瞬間である
 * @retval			false	押された瞬間でない
 */
bool DS3_IsClicked(DS3_ButtonID btn);


/**
 * @brief	該当のボタンが離された瞬間かのアクセサ
 *
 * @param [in]		btn		取得する対象のボタンを指定する
 * @return					離された瞬間の１フレームだけtrueが返る。それ以外はfalse
 * @retval			true	放された瞬間である
 * @retval			false	放された瞬間でない
 */
bool DS3_IsReleased(DS3_ButtonID btn);


/**
 * @brief	内部管理されているbuttonsState[]へのアクセサ
 *
 * 			押されている時間を取得する関数です。ここでいう時間とは、あくまでカウントされた回数で、
 * 			このver.1.0.0においてはUpdate関数が呼び出される毎にカウントされるのでframe数として捉え
 * 			ることが可能です。これを用いて長押しなどを検出できます。
 * 			離された瞬間は-1が格納されていることに注意してください。
 *
 * @note	将来的にはUSARTでの受信イベントハンドラ実行時毎にカウントされるように変更する可能性有り
 *
 * @param [in]		btn		取得する対象のボタンを指定する
 * @return					押されている間のカウント数。離された瞬間は-1が1度格納された後、
 * @retval			-1		ボタンは離された瞬間
 * @retval			0		ボタンは離されていて押されていない
 * @retval			1以上	押され続けている間のカウント数
 */
short DS3_GetPressedCount(DS3_ButtonID btn);


/**
 * @brief	内部管理されているsticksState[]へのアクセサ
 *
 * 			ジョイスティックの傾き具合を取得するための関数です。
 * 			X座標(水平方向)は右に増加、左に減少し、Y座標(垂直方向)は下に増加、上に減少します。
 * 			いずれの値も0x00～0x7f (10進で0～127)であり、中央（ニュートラル時）には0x40 (10進
 * 			で64)が格納されます。閾値による補正がかかります。補正がかからない情報がほしいとき
 * 			は、閾値を０に設定するか、CmdDatやstickStateを参照する必要があります。
 *
 * @param [in]		btn		取得する対象のジョイスティックをDS3_JoystickID型で指定する
 * @return					指定されたジョイスティックの傾き具合。
 */
int DS3_GetJoystick(DS3_JoystickID joy);


/**
 * @brief	左のジョイスティックがニュートラルであるかを返す
 *
 * 			中央からの傾き具合がXYどちらもしきい値以内の値であるときにニュートラルとみなす
 *
 * @return					左のジョイスティックの状態を返す
 * @retval			true	ニュートラルである
 * @retval			false	ニュートラルでない
 */
bool DS3_IsNeutralLJoystick();


/**
 * @brief	右のジョイスティックがニュートラルであるかを返す
 *
 * 			中央からの傾き具合がXYどちらもしきい値以内の値であるときにニュートラルとみなす
 *
 * @return					右のジョイスティックの状態を返す
 * @retval			true	ニュートラルである
 * @retval			false	ニュートラルでない
 */
bool DS3_IsNeutralRJoystick();


/// ジョイスティックでニュートラルとみなす傾き具合の閾値(SBDBT側でも閾値を設定してそう)
#define DS3_JOY_THRESHOLD			(0)


/**
 * @brief	内部管理されているCmdData[]へのアクセサ
 *
 * 			通常使う必要がありません。SDBTBから送られる生のデータが欲しいときに使用できます。
 *
 * @note
 * 			unsafeモード時には、指定された添字が0～7かを確認しなくなります。
 *
 * @param [in]		i		取得したい値の添字
 * @return					該当するCmdData[]
 */
unsigned char DS3_GetRawData(int i);


#endif
