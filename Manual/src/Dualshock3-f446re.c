/**
 * @file	Dualshock3.c
 * @version	2.0.1
 *
 * 			このライブラリの使い方については同梱するDualshock3.h及びmain.cを参照のこと
 *
 * @par			内部挙動について
 * 			ここでは簡単にだが、内部的な挙動などについて記述する。まず見てわかるようにSBDBTとはUSARTを用いて通信
 * 			を行うが、USART1/2を.hのマクロ定義で選択できる※現在初期化ではこのUSARTとそのDMAまわりの初期化しかし
 * 			ていない。DMAでは、USARTで受信するたびにイベントハンドラ関数DMA1_Channel5_IRQHandlerが呼び出される。
 * 			このイベントハンドラ内にてボタンが押されているかいないかを受信したデータ(RawData)を解析する。ここで、
 * 			各ボタンにつき押されているいない(buttonsIsPressed[])のみを格納する。ここで取得したデータをもとに、毎
 *			ループで呼び出されることを想定している更新関数Updateにて、押し離しを検知している。無駄にループが重な
 *			っている気もするが、安定版完成のためにこうした。次期ver.2.xとかでは効率化を考えたいがかったるい。
 *
 *
 *	@par		ライブラリ設計について（独自型とその定数）
 *			C言語ではenum型を整数型と同一とみなせてしまうという仕様があり、C++では厳密な
 *			型判断が適用されコンパイルエラーになる場合でも、Cでは実行できてしまうケースが
 *			存在するみたいだった。
 *			@code
 *				typedef enum {E_A, E_B, E_C} EnumType;
 *				void func(EnumType e);
 *
 *				void main(){
 *					func(E_A);	// 期待される正常な呼び出し
 *					func(10);	// 想定されない呼び出し（C++だとコンパイルエラー）
 *				}
 *			@endcode
 *			当初はボタン及びジョイスティックの識別子として、enumをtypedefしたDS3_ButtonID
 *			型やDS3_JoystickID型の定数DS3_UPなどを用いていたが、上述の仕様により混同できて
 *			しまうだけでなく上のコード例のように想定外の整数などを含めることができてしまう。
 *  		ボタンの識別子に定義された定数を用いて配列にアクセスする設計だったので、これで
 *  		は異常な値が渡されたとき配列の範囲外にアクセスしてしまう。これを防ぐためには、
 *  			1. ソースコードを厳密な型チェックが適用されるC++にする
 *  			2. 諦めて、ライブラリ利用者が気をつける
 *  			3. 諦めて、でも諦めきれないので足掻く
 *  		の3つの方法が挙げられるが現環境でライブラリ利用者にC++化を強制するのもいまいち
 *			だと思ったので、3番目の方法を取ることにした。具体的には、enum型のみを保持する
 *			struct型をtypedefしてラップすることにより型の問題を解決することにした。この構
 *			造体型のconstなグローバル変数を定数として用いる。メモリの無駄遣いにはなるかも
 *			れないが想定されない入力は防げる。あまり聞かない手法なので邪道かもしれないとい
 *			うか、致命的な欠点があるかもしれない。他にもdefineマクロを使ったり、ifで範囲チ
 *			ェックしたりといった方法がありそうだし、場合によってはそのうち変えるかも。
 *
 *
 * @TODO	USART2_Configurationが未実装。面倒だからとりあえず後で
 * 			というか、コンフィグ回りがガバガバ設計なので、しっかり整理したほうがいいよなぁ……
 * 			そのうち、整備されたUSARTライブラリを利用するような形にするべき
 * @TODO	本来は、基本的な処理をイベントハンドラで行い、フラグのリセットのみを毎ループのUpdateで行いたかったが
 * 			思ったように動作しなかったので、2016-11-17付けのOLDファイルにバックアップ後一新した。
 *
 * @author	春日野穹
 * @since	2016/11/05
 * @date	2016/11/12	ver0.1.0	イベントハンドラが作動しなかったのでUpdateに全ての処理を入れていた
 *			2016/11/14				イベントハンドラが動作するようになったので設計し直し
 *			2016/11/17	ver1.0.0β	イベントハンドラ内にbuttonPressedTimeProcとか色々な処理を行なおうとしたが、
 *									バグの修正が面倒になったので、イベントハンドラ内では押されているか否かの２値
 *									のみを判別するようにして、btnPressedTimeProc等はUpdate内で処理するようにした
 *									とりあえず安定版としてひとまず完成させた
 *			2017/04/20	ver.2.0.0	gitでの公開するに先立って、従来のenum型をstruct型でラップするようにしたり、
 *									無駄っぽい機能を色々消したりとかして.hや.cをシンプルにした。
 *
 *
 */

#include "stm32f4xx.h"
#include "Dualshock3.h"
#include "SimpleUSART.h"						// 開発時デバッグ用

#define RX_BUFFER 8

static uint8_t CmdData[RX_BUFFER];				//!< Dualshock3から受け取ったコマンドが代入される配列

// TODO: PressedTimeとかに名前変えたほうが良い気がする
static int buttonsStates[_DS3_ARRAY_NUM_] = {0};
static uint8_t sticksStates[_DS3_JOY_NUM_] = {0x40, 0x40, 0x40, 0x040};

static bool buttonsIsPressed[_DS3_ARRAY_NUM_] = {false};    //!< 各ボタンが押されているか否かを格納する配列
static bool buttonsIsClicked[_DS3_ARRAY_NUM_] = {false};    //!< 各ボタンが押された瞬間にのみ１回だけtrueになりそれ以外はfalseな配列
static bool buttonsIsReleased[_DS3_ARRAY_NUM_] = {false};   //!< 各ボタンが離された瞬間にのみ１回だけtrueになりそれ以外はfalseな配列


/// 各ボタンに対応するCmdDataでのバイト位置
static const uint8_t DS3_BTNS_DATMAP_BYTE[_DS3_ARRAY_NUM_] = {
	2, 2, 2, 2, 2, 2,/* 0, 0,*/ 2, 2, 2, 1, 1, 1, 1, 1
};

//// 【旧】各ボタンに対応するCmdDataでのビット位置というかビットマスク（UP～LEFTとSELECTとSTARTはビットを共有してるっぽい）
//static const uint8_t DS3_BTNS_DATMAP_BIT[_DS3_ARRAY_NUM_] = {
//	0x01, 0x04, 0x02, 0x08, 0x03, 0x0C,/* 0x00, 0x00,*/
//	0x10, 0x20, 0x40, 0x01, 0x04, 0x10, 0x02, 0x08,
//};

/// 各ボタンに対応するCmdDataでの検証するべきビットマスク
static const uint8_t DS3_BTNS_DATMAP_BITMASK[_DS3_ARRAY_NUM_] = {
	0x03, 0x0C, 0x03, 0x0C, 0x03, 0x0C,/* 0x00, 0x00,*/
	0x10, 0x40, 0x20, 0x01, 0x04, 0x10, 0x02, 0x08,
};

/// 各ボタンに対応するCmdDataのビットマスクとった後に立ってるべきbit
static const uint8_t DS3_BTNS_DATMAP_FLAGBIT[_DS3_ARRAY_NUM_] = {
	0x01, 0x04, 0x02, 0x08, 0x03, 0x0C,/* 0x00, 0x00,*/
	0x10, 0x40, 0x20, 0x01, 0x04, 0x10, 0x02, 0x08,
};


const _DS3_ButtonIDs DS3_BTN = {{_DS3_UP}, {_DS3_RIGHT}, {_DS3_DOWN}, {_DS3_LEFT}, {_DS3_START}, {_DS3_SELECT}, {_DS3_DELTA}, {_DS3_CIRCLE}, {_DS3_CROSS}, {_DS3_SQUARE}, {_DS3_L2}, {_DS3_R2}, {_DS3_L1}, {_DS3_R1}};
const _DS3_JoystickIDs DS3_JOY = {{_DS3_L3X}, {_DS3_L3Y}, {_DS3_R3X}, {_DS3_R3Y}};


static void AnalyzeRawData();				// CmdData(RawData)の解析関数


// Dualshock3の初期化関数
void DS3_Init() {
	// こんな方法するなら正直関数化した方が良い気がするけどとりあえずこれで済ませてみる
	// TODO: USART1以外未整備
#if defined DS3_USE_USART1
	// TX:PA9	RX:PA10		DMA2-2-4 or DMA2-5-4
	USART_TypeDef*		const USARTx				= USART1;
	uint8_t				const GPIO_AF_USARTx		= GPIO_AF_USART1;
	GPIO_TypeDef*		const usartTxGpioPort		= GPIOA;
	uint16_t			const usartTxGpioPin		= GPIO_Pin_9;
	uint8_t				const usartTxGpioPinSource	= GPIO_PinSource9;
	GPIO_TypeDef*		const usartRxGpioPort		= GPIOA;
	uint16_t			const usartRxGpioPin		= GPIO_Pin_10;
	uint8_t				const usartRxGpioPinSource	= GPIO_PinSource10;
	DMA_Stream_TypeDef*	const DMAy_Streamx			= DMA2_Stream2;
	uint32_t			const DMA_Channel_z			= DMA_Channel_4;
	uint8_t				const DMAy_Streamx_IRQn		= DMA2_Stream2_IRQn;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_DMA2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
#elif defined DS3_USE_USART2
	// TX:PA2	RX:PA3		DMA1-5-4
	USART_TypeDef*		const USARTx				= USART2;
	uint8_t				const GPIO_AF_USARTx		= GPIO_AF_USART2;
	GPIO_TypeDef*		const usartTxGpioPort		= GPIOA;
	uint16_t			const usartTxGpioPin		= GPIO_Pin_2;
	uint8_t				const usartTxGpioPinSource	= GPIO_PinSource2;
	GPIO_TypeDef*		const usartRxGpioPort		= GPIOA;
	uint16_t			const usartRxGpioPin		= GPIO_Pin_3;
	uint8_t				const usartRxGpioPinSource	= GPIO_PinSource3;
	DMA_Stream_TypeDef*	const DMAy_Streamx			= DMA1_Stream5;
	uint32_t			const DMA_Channel_z			= DMA_Channel_4;
	uint8_t				const DMAy_Streamx_IRQn		= DMA1_Stream5_IRQn;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_DMA1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
#elif defined DS3_USE_USART3
	// TX:PC10	RX:PC11		DMA1-1-4
	USART_TypeDef*		const USARTx				= USART3;
	uint8_t				const GPIO_AF_USARTx		= GPIO_AF_USART3;
	GPIO_TypeDef*		const usartTxGpioPort		= GPIOC;
	uint16_t			const usartTxGpioPin		= GPIO_Pin_10;
	uint8_t				const usartTxGpioPinSource	= GPIO_PinSource10;
	GPIO_TypeDef*		const usartRxGpioPort		= GPIOC;
	uint16_t			const usartRxGpioPin		= GPIO_Pin_11;
	uint8_t				const usartRxGpioPinSource	= GPIO_PinSource11;
	DMA_Stream_TypeDef*	const DMAy_Streamx			= DMA1_Stream1;
	uint32_t			const DMA_Channel_z			= DMA_Channel_4;
	uint8_t				const DMAy_Streamx_IRQn		= DMA1_Stream1_IRQn;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_DMA1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
#elif defined DS3_USE_USART6
	// TX:PC6	RX:PC7		DMA2-1-5 or DMA2-2-5
	USART_TypeDef*		const USARTx				= USART6;
	uint8_t				const GPIO_AF_USARTx		= GPIO_AF_USART6;
	GPIO_TypeDef*		const usartTxGpioPort		= GPIOC;
	uint16_t			const usartTxGpioPin		= GPIO_Pin_7;
	uint8_t				const usartTxGpioPinSource	= GPIO_PinSource7;
	GPIO_TypeDef*		const usartRxGpioPort		= GPIOC;
	uint16_t			const usartRxGpioPin		= GPIO_Pin_6;
	uint8_t				const usartRxGpioPinSource	= GPIO_PinSource6;
	DMA_Stream_TypeDef*	const DMAy_Streamx			= DMA2_Stream1;
	uint32_t			const DMA_Channel_z			= DMA_Channel_5;
	uint8_t				const DMAy_Streamx_IRQn		= DMA2_Stream1_IRQn;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_DMA2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
#else
	#error LyricalLib.DS3:UnselectedError「ふぇぇ…使用するUSARTを指定しなくちゃだめだよぉ」
#endif

	// GPIOまわり
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = usartTxGpioPin;	// TX
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(usartTxGpioPort, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = usartRxGpioPin;	// RX
	GPIO_Init(usartRxGpioPort, &GPIO_InitStructure);

	GPIO_PinAFConfig(usartTxGpioPort, usartTxGpioPinSource, GPIO_AF_USARTx);
	GPIO_PinAFConfig(usartRxGpioPort, usartRxGpioPinSource, GPIO_AF_USARTx);

	// DMA
	DMA_DeInit(DMAy_Streamx);
	DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_Channel = DMA_Channel_z;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&USARTx->DR;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)CmdData;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = RX_BUFFER;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMAy_Streamx, &DMA_InitStructure);
	DMA_ITConfig(DMAy_Streamx, DMA_IT_TC, ENABLE);
	DMA_Cmd(DMAy_Streamx, ENABLE);

	// USART
	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_Init(USARTx, &USART_InitStructure);
	USART_DMACmd(USARTx, USART_DMAReq_Rx, ENABLE);
	USART_Cmd(USARTx, ENABLE);

#ifdef DS3_USE_INTERRUPT
	// NVIC：ここではIRQHandlerとかの#defineしたほうが良いかも
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = DMAy_Streamx_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
#endif
}


// Dualshock3の更新関数。これを１フレーム毎に（while中やTIMx_IRQHandlerで）呼び出してください
// 現状やっていることはただのフラグリセットというか瞬間フレームのみ取りたい値の補正
//  → ver.1.0.x では安定版完成のためにとりあえず適当に処理してしまった。冒頭のドキュメント参照
void DS3_Update(){
#ifndef DS3_USE_INTERRUPT
	if (CmdData[0] == 0x80){				// スタートビット確認
		AnalyzeRawData();
	}
#endif

	for (_DS3_ButtonID btnid = _DS3_ARRAY_START_; btnid < _DS3_ARRAY_NUM_; btnid++) {
		buttonsIsClicked[btnid] = false;
		buttonsIsReleased[btnid] = false;
		if (buttonsIsPressed[btnid]) {
			// 押されている場合
			buttonsStates[btnid]++;
			if (buttonsStates[btnid] == 1) {
				buttonsIsClicked[btnid] = true;
			}
		} else {
			// 押されていない場合
			if (buttonsStates[btnid] > 0) {
				buttonsStates[btnid] = -1;
				buttonsIsReleased[btnid] = true;
			} else {
				buttonsStates[btnid] = 0;
			}
		}
	}
}



// CmdData(RawData)の解析関数
static void AnalyzeRawData(){
	// ボタンとかを列挙してもいいんだけど面倒なのでやっぱりforで処理。じゃないとDS3_BTNS_DATMAPの意味が無いですしおすし
	// ボタンの押されているいないの処理はbuttonPressedTimeProc関数に分離
	for(_DS3_ButtonID btnid = _DS3_ARRAY_START_; btnid < _DS3_ARRAY_NUM_; btnid++){
		bool f = (CmdData[DS3_BTNS_DATMAP_BYTE[btnid]] & DS3_BTNS_DATMAP_BITMASK[btnid]) == DS3_BTNS_DATMAP_FLAGBIT[btnid];
		buttonsIsPressed[btnid] = f;
//		buttonsIsPressed_Resetflg |= f >> btn;
	}

	// ジョイスティック処理(手抜きだけどいいよね）
	sticksStates[_DS3_L3X] = CmdData[3];
	sticksStates[_DS3_L3Y] = CmdData[4];
	sticksStates[_DS3_R3X] = CmdData[5];
	sticksStates[_DS3_R3Y] = CmdData[6];
}



// ボタンの押されている状態(押されているカウント)を取得する
short DS3_GetPressedCount(DS3_ButtonID btn){
	return buttonsStates[btn.id];
}

// ジョイスティックの傾きを取得する
int DS3_GetJoystick(DS3_JoystickID joy){
#if DS3_JOY_THRESHOLD <= 0
	return sticksStates[joy.id];
#else
	return (-DS3_JOY_THRESHOLD <= sticksStates[joy.id]-0x40) && (sticksStates[joy.id]-0x40 <= DS3_JOY_THRESHOLD)
			? 0x40 : sticksStates[joy.id];
#endif
}

// 左ジョイスティックがニュートラルかを取得する
bool DS3_IsNeutralLJoystick(){
#if DS3_JOY_THRESHOLD <= 0
	return sticksStates[DS3_JOY.L3X.id]==0x40 && sticksStates[DS3_JOY.L3Y.id]==0x40;
#else
	return (-DS3_JOY_THRESHOLD <= sticksStates[DS3_JOY.L3X.id]-0x40) &&
			(+DS3_JOY_THRESHOLD >= sticksStates[DS3_JOY.L3X.id]-0x40) &&
			(-DS3_JOY_THRESHOLD <= sticksStates[DS3_JOY.L3Y.id]-0x40) &&
			(+DS3_JOY_THRESHOLD >= sticksStates[DS3_JOY.L3Y.id]-0x40) ? true : false;
#endif
}

// 右ジョイスティックがニュートラルかを取得する
bool DS3_IsNeutralRJoystick(){
#if DS3_JOY_THRESHOLD <= 0
	return sticksStates[DS3_JOY.R3X.id]==0x40 && sticksStates[DS3_JOY.R3Y.id]==0x40;
#else
	return (-DS3_JOY_THRESHOLD <= sticksStates[DS3_JOY.R3X.id]-0x40) &&
			(+DS3_JOY_THRESHOLD >= sticksStates[DS3_JOY.R3X.id]-0x40) &&
			(-DS3_JOY_THRESHOLD <= sticksStates[DS3_JOY.R3Y.id]-0x40) &&
			(+DS3_JOY_THRESHOLD >= sticksStates[DS3_JOY.R3Y.id]-0x40) ? true : false;
#endif
}


// 押されている間はtrueが返り続け、そうでないときはfalseが返る
bool DS3_IsPressed(DS3_ButtonID btn) {
	return buttonsIsPressed[btn.id];
}

// 押された瞬間の１フレームだけtrueが返る。それ以外はfalse
bool DS3_IsClicked(DS3_ButtonID btn){
	return buttonsIsClicked[btn.id];
}

// 離された瞬間の１フレームだけtrueが返る。それ以外はfalse
bool DS3_IsReleased(DS3_ButtonID btn){
	return buttonsIsReleased[btn.id];
}


inline uint8_t DS3_GetRawData(int i){
#ifdef DS3_UNSAFE
	return CmdData[i];
#else
	return (0 <= i && i < 8) ? CmdData[i] : 0;
#endif
}


// DMAによるイベントハンドラ（割り込み）
// TODO: USART1以外未整備
#ifdef DS3_USE_INTERRUPT
#if defined DS3_USE_USART1
	void DMA2_Stream2_IRQHandler(void) {
		if (DMA_GetITStatus(DMA2_Stream2, DMA_IT_TCIF2)){
			debug("男の娘わぁい");
			if (CmdData[0] == 0x80){				// スタートビット確認
				AnalyzeRawData();
			}
			DMA_ClearITPendingBit(DMA2_Stream2, DMA_IT_TCIF2);
		}
	}
#elif defined DS3_USE_USART2
	void DMA1_Stream5_IRQHandler(void) {
		if (DMA_GetITStatus(DMA1_Stream5, DMA_IT_TCIF5)){
			debug("男の娘わぁい");
			if (CmdData[0] == 0x80){				// スタートビット確認
				AnalyzeRawData();
			}
			DMA_ClearITPendingBit(DMA1_Stream5, DMA_IT_TCIF5);
		}
	}
#elif defined DS3_USE_USART3
	void DMA1_Stream1_IRQHandler(void) {
		if (DMA_GetITStatus(DMA1_Stream1, DMA_IT_TCIF1)){
			debug("男の娘わぁい");
			if (CmdData[0] == 0x80){				// スタートビット確認
				AnalyzeRawData();
			}
			DMA_ClearITPendingBit(DMA1_Stream1, DMA_IT_TCIF1);
		}
	}
#elif defined DS3_USE_USART6
	void DMA2_Stream1_IRQHandler(void) {
		if (DMA_GetITStatus(DMA2_Stream1, DMA_IT_TCIF1)){
			debug("男の娘わぁい");
			if (CmdData[0] == 0x80){				// スタートビット確認
				AnalyzeRawData();
			}
			DMA_ClearITPendingBit(DMA2_Stream1, DMA_IT_TCIF1);
		}
	}
#endif
#endif
