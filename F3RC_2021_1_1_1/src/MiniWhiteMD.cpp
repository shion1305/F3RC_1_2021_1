/*
 * MiniWhiteMD.cpp
 *
 *  Created on: 2019/04/13
 * Last Update: 2019/09/02
 *      Author: G.u.i.
 */

#include "MiniWhiteMD.hpp"

#define PWM_RESOLUTION 2000		// ここの数値を変えるとPWMの分解能が変わる
#define PWM_NEUTRAL PWM_RESOLUTION/2		// PWM分解能の半分 つまりDuty比50%でモータが停止する

void MiniWhiteMD::initMd(
	GPIO_TypeDef *port_PWM,
	uint16_t pin_PWM,
	TIM_TypeDef *TIM_num,
	uint16_t TIM_ch
)
{
// とりあえずメンバ変数に引数をコピー(美しくないのでどうにかしたい)
	port_PWM_m = port_PWM;
	pin_PWM_m = pin_PWM;
	TIM_num_m = TIM_num;
	TIM_ch_m = TIM_ch;

// 初期化用構造体を片っ端から宣言
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

// PWMを吐き出すタイマーのAPB1とAPB2にクロックを供給
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM3 | RCC_APB1Periph_TIM4 | RCC_APB1Periph_TIM5 | RCC_APB1Periph_TIM6 | RCC_APB1Periph_TIM7 | RCC_APB1Periph_TIM12 | RCC_APB1Periph_TIM13 | RCC_APB1Periph_TIM14, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 | RCC_APB2Periph_TIM8 | RCC_APB2Periph_TIM9 | RCC_APB2Periph_TIM10 | RCC_APB2Periph_TIM11, ENABLE);

// GPIOのAHB1にクロックを供給
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB |RCC_AHB1Periph_GPIOC, ENABLE);

// GPIO設定
	GPIO_InitStructure.GPIO_Pin = pin_PWM_m;		// PWMに使う方のピン
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;		// PWM吐き出しモードにする
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;		// プッシュプルで出力させる
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		// 特にプル抵抗は不要
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		// 脳死
	GPIO_Init(port_PWM_m, &GPIO_InitStructure);		// 構造体からピン設定を反映させる
	GPIO_PinAFConfig(port_PWM_m, getGpioPinSource(pin_PWM_m), getGpioAf(TIM_num_m));		// 自前の関数を用いてピン設定と周辺機能を関連付け

// タイマーの設定
	TIM_TimeBaseStructure.TIM_Period = PWM_RESOLUTION;		// PWM分解能を周期とする
	TIM_TimeBaseStructure.TIM_Prescaler = 1;		// 分解能がデカいのでプリスケーラは要らない
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;		// 分解能がデカいのでクロック分周は要らない
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;		// カウントアップモードでおk
	//TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM_num_m, &TIM_TimeBaseStructure);		// 構造体からタイマー設定を反映させる

// タイマー出力の設定
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;		// 一般的なPWMのモードにする
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;		// PWM吐き出しを有効にする
	//TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Disable;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;		// これを変えると出力が反転しそう(小学生並みの感想)
	TIM_OCInitStructure.TIM_Pulse = 0;		// これよくわかんない

// タイマー出力先のピンを設定(脳筋コードなので誰か改善して)
	switch(TIM_ch_m)
	{
	case 1:
		TIM_OC1Init(TIM_num_m, &TIM_OCInitStructure);
		TIM_OC1PreloadConfig(TIM_num_m, TIM_OCPreload_Disable);
		break;
	case 2:
		TIM_OC2Init(TIM_num_m, &TIM_OCInitStructure);
		TIM_OC2PreloadConfig(TIM_num_m, TIM_OCPreload_Disable);
		break;
	case 3:
		TIM_OC3Init(TIM_num_m, &TIM_OCInitStructure);
		TIM_OC3PreloadConfig(TIM_num_m, TIM_OCPreload_Disable);
		break;
	case 4:
		TIM_OC4Init(TIM_num_m, &TIM_OCInitStructure);
		TIM_OC4PreloadConfig(TIM_num_m, TIM_OCPreload_Disable);
		break;
	}

	setMotor(0);		// 暴走防止用 これが無いとモータがじゃじゃ馬になる

	TIM_Cmd(TIM_num_m, ENABLE);		// ここでタイマーを有効にする

// 扱いが少々厄介なTIM1とTIM8のPWMを解放する さぁ萌えよ
	if(TIM_num_m == TIM1)
		TIM1->BDTR |= TIM_BDTR_MOE;
	if(TIM_num_m == TIM8)
		TIM8->BDTR |= TIM_BDTR_MOE;

	return;
}

uint8_t MiniWhiteMD::getGpioAf(
	TIM_TypeDef *TIM
)
{
// なんだこの脳筋コードは(驚愕)
// GPIO_AFを取得するのダルすぎません？
	if(TIM == TIM1)
		return GPIO_AF_TIM1;
	if(TIM == TIM2)
		return GPIO_AF_TIM2;
	if(TIM == TIM3)
		return GPIO_AF_TIM3;
	if(TIM == TIM4)
		return GPIO_AF_TIM4;
	if(TIM == TIM5)
		return GPIO_AF_TIM5;
	if(TIM == TIM8)
		return GPIO_AF_TIM8;
	if(TIM == TIM9)
		return GPIO_AF_TIM9;
	if(TIM == TIM10)
		return GPIO_AF_TIM10;
	if(TIM == TIM11)
		return GPIO_AF_TIM11;
	if(TIM == TIM12)
		return GPIO_AF_TIM12;
	if(TIM == TIM13)
		return GPIO_AF_TIM13;
	if(TIM == TIM14)
		return GPIO_AF_TIM14;

	return 0;
}

uint16_t MiniWhiteMD::getGpioPinSource(
	uint16_t Pin
)
{
// なんだこの脳筋コードは(驚愕)
// GPIOのPinSourceを取得するのダルすぎません？
	switch(Pin)
	{
	case GPIO_Pin_0:
		return GPIO_PinSource0;
	case GPIO_Pin_1:
			return GPIO_PinSource1;
	case GPIO_Pin_2:
			return GPIO_PinSource2;
	case GPIO_Pin_3:
			return GPIO_PinSource3;
	case GPIO_Pin_4:
			return GPIO_PinSource4;
	case GPIO_Pin_5:
			return GPIO_PinSource5;
	case GPIO_Pin_6:
			return GPIO_PinSource6;
	case GPIO_Pin_7:
			return GPIO_PinSource7;
	case GPIO_Pin_8:
			return GPIO_PinSource8;
	case GPIO_Pin_9:
			return GPIO_PinSource9;
	case GPIO_Pin_10:
			return GPIO_PinSource10;
	case GPIO_Pin_11:
			return GPIO_PinSource11;
	case GPIO_Pin_12:
			return GPIO_PinSource12;
	case GPIO_Pin_13:
			return GPIO_PinSource13;
	case GPIO_Pin_14:
			return GPIO_PinSource14;
	case GPIO_Pin_15:
			return GPIO_PinSource15;
	}

	return 0;
}

void MiniWhiteMD::setCompare(
	uint32_t val
)
{
	switch(TIM_ch_m)
	{
	case 1:
		TIM_SetCompare1(TIM_num_m, val);
		break;
	case 2:
		TIM_SetCompare2(TIM_num_m, val);
		break;
	case 3:
		TIM_SetCompare3(TIM_num_m, val);
		break;
	case 4:
		TIM_SetCompare4(TIM_num_m, val);
		break;
	default:
		break;
	}

	return;
}

void MiniWhiteMD::setMotor(
	int output
)
{
	setCompare(output + PWM_NEUTRAL);

	return;
}



