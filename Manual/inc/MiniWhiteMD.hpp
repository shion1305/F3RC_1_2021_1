/*
 * MiniWhiteMD.hpp
 *
 *  Created on: 2019/04/13
 * Last Update: 2019/09/02
 *      Author: G.u.i.
 */

#ifndef MINIWHITEMD_H_
#define MINIWHITEMD_H_

#include "stm32f4xx.h"

class MiniWhiteMD
{
public:
	void initMd(GPIO_TypeDef *port_PWM, uint16_t pin_PWM, TIM_TypeDef *TIM_num, uint16_t TIM_ch);
	void setMotor(int output);

private:
	uint8_t getGpioAf(TIM_TypeDef *TIM);
	uint16_t getGpioPinSource(uint16_t Pin);
	void setCompare(uint32_t val);

	GPIO_TypeDef *port_PWM_m;
	uint16_t pin_PWM_m;
	uint16_t TIM_ch_m;
	TIM_TypeDef *TIM_num_m;
};

#endif /* MINIWHITEMD_H_ */
